import unittest

import numpy as np
from src import physics
from src import helpers

def get_alpha_m(y):
    return -3./2. - np.sqrt(9./4. + 4./y)

def get_alpha_p(y):
    return -3./2. + np.sqrt(9./4. + 4./y)

class Tests(unittest.TestCase):

    def test_get_y_m1(self):
        y = 1.
        alpha = get_alpha_m(y)
        self.assertAlmostEqual(physics.get_y(alpha), y)

    def test_get_y_p1(self):
        y = 1.
        alpha = get_alpha_p(y)
        self.assertAlmostEqual(physics.get_y(alpha), y)

    def test_get_y_m2(self):
        y = 2.
        alpha = get_alpha_m(y)
        self.assertAlmostEqual(physics.get_y(alpha), y)

    def test_get_y_many_values_m(self):
        ys = [0.01, 0.1, 1., 10.]
        for y in ys:
            alpha = get_alpha_m(y)
            self.assertAlmostEqual(physics.get_y(alpha), y)

    def test_get_y_many_values_p(self):
        ys = [0.01, 0.1, 1., 10.]
        for y in ys:
            alpha = get_alpha_p(y)
            self.assertAlmostEqual(physics.get_y(alpha), y)

    def test_get_eps_avg_from_spec(self):
        theta = 1e-2
        epss = np.logspace(-4 + np.log10(theta), +1.5 + np.log10(theta), 400)
        bbs = [helpers.blackbody(eps, theta) for eps in epss]
        eps_avg = helpers.get_eps_avg_from_spec(epss, bbs)
        self.assertAlmostEqual(eps_avg, 2.70*theta, places=2)

if __name__ == '__main__':
    unittest.main()
