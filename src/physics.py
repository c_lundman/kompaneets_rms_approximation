from scipy.optimize import root_scalar

# Constants
m_e = 9.1093897e-28
m_p = 1.6726231e-24

def get_theta_RMS(beta):
    # From this equation: eps_d_max = 3*theta_RMS = 0.2*beta^2
    # theta_RMS = (1./15.)*beta*beta
    theta_RMS = (1./15.)*beta*beta/1.5
    return theta_RMS

def get_R_theta(beta, theta_u):
    # Simply the ratio of temperatures
    theta_RMS = get_theta_RMS(beta)
    R_theta = theta_RMS/theta_u
    return R_theta

def get_eps_u_avg_from_theta_u(theta_u):
    # Should this be larger by a factor ~ few?
    eps_u_avg = 3.*theta_u
    return eps_u_avg

def get_eps_d_min_from_theta_u(theta_u):
    eps_d_min = get_eps_u_avg_from_theta_u(theta_u)
    eps_d_min *= 1.5
    return eps_d_min

def get_eps_d_max_from_beta(beta):
    theta_RMS = get_theta_RMS(beta)
    return 3.*theta_RMS
#

def get_M_sq(beta, theta_u, zeta):
    M_sq = (3./2.)*(m_p/m_e)*beta*beta/theta_u/zeta
    return M_sq

def get_eps_d_avg_over_eps_u_avg(M_sq):
    return (8.*M_sq - 1.)/7.*((M_sq + 6.)/7./M_sq)

def should_equal_zero(alpha, eps_d_min, eps_d_max, eps_d_over_eps_u):
    # The root (i.e. correct alpha) is found when this function returns zero
    if alpha == 3. or alpha == 4.:
        alpha += 0.00001
    A = (alpha + 3.)/(alpha + 4.)*((eps_d_max/eps_d_min)**(alpha+4.) - 1.)/((eps_d_max/eps_d_min)**(alpha+3.) - 1.)
    return A-eps_d_over_eps_u

def solution_is_not_bracketed(alpha_min, alpha_max, eps_min, eps_max, eps_d_over_eps_u):
    s_min = should_equal_zero(alpha_min, eps_min, eps_max, eps_d_over_eps_u)
    s_max = should_equal_zero(alpha_max, eps_min, eps_max, eps_d_over_eps_u)
    if s_min < 0. and s_max < 0.:
        return True
    elif s_min > 0. and s_max > 0.:
        return True
    else:
        return False

def get_alpha(eps_min, eps_max, eps_d_over_eps_u):
    # Minimum and maximum energies in the power law
    alpha_min = -10.
    alpha_max = +10.

    if solution_is_not_bracketed(alpha_min, alpha_max, eps_min, eps_max, eps_d_over_eps_u):
        print("Error: the alpha solution is not bracketed")
        print("       is the spectrum really a power law for these parameters?")
        raise SystemExit

    solution = root_scalar(should_equal_zero, args=(eps_min, eps_max, eps_d_over_eps_u), x0=1., method='brentq', bracket=(alpha_min, alpha_max))
    if not solution.converged:
        print('Error: root finding did not converge')
        raise SystemExit
    alpha = solution.root
    return alpha

def get_y(alpha):
    # Find the y-parameter from alpha
    A = (alpha + 3./2.)*(alpha + 3./2.) - 9./4.
    y = 4./A
    return y
