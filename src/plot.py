from matplotlib import pyplot as plt
import numpy as np
import helpers

def make_parameter_texts(beta, theta_u, zeta, tau, tau_theta_RMS, R_theta, y):
    texts = [r"$\beta = {0:s}$".format(helpers.nice_notation(beta)),
             r"$\theta_u = {0:s}$".format(helpers.nice_notation(theta_u)),
             r"$\zeta = {0:s}$".format(helpers.nice_notation(zeta)),
             r"$\tau = {0:s}$".format(helpers.nice_notation(tau)),
             r"",
             r"$\tau \theta_\mathrm{RMS}$" + r"$ = {0:s}$".format(helpers.nice_notation(tau_theta_RMS)),
             r"$R_\theta = {0:s}$".format(helpers.nice_notation(R_theta)),
             r"$y = {0:s}$".format(helpers.nice_notation(y))]
    return texts

def make_spectrum(eps_d_min, eps_d_max, alpha, add_blackbodies=True):
    epss = np.logspace(-7, -1, 500)

    theta_min = eps_d_min/3./1.3
    theta_max = eps_d_max/3./1.3

    eps_min_break = eps_d_min
    eps_max_break = eps_d_max

    nuFnus = []
    for eps in epss:
        if eps < eps_min_break:
            if add_blackbodies:
                nuFnu = helpers.blackbody(eps, theta_min)
            else:
                nuFnu = 0.
        elif eps < eps_max_break:
            nuFnu = (eps/eps_min_break)**(alpha+4.)
        else:
            if add_blackbodies:
                A = (eps_max_break/eps_min_break)**(alpha+4.)
                nuFnu = helpers.blackbody(eps, theta_max, A=A)
            else:
                nuFnu = 0.
        nuFnus.append(nuFnu)

    # Find the average energy from the spectrum
    eps_d_avg_from_spec = helpers.get_eps_avg_from_spec(epss, nuFnus)
    return epss, nuFnus, eps_d_avg_from_spec

def plot_spectrum(epss, nuFnus, eps_min, eps_d_avg, eps_d_avg_from_spec, texts, filename=None):

    plt.rcParams.update({'font.size': 14})
    plt.figure(figsize=(7, 6))

    y_max = 30.*max(nuFnus)
    y_min = 1e-5*y_max
    y_step = 2.2

    y = y_max/y_step
    for text in texts:
        plt.text(1.4*epss[0], y, text)
        y /= y_step

    plt.vlines(eps_min, y_min, y_max, color='m', alpha=.2, linestyle=':', label=r'$\bar{\epsilon}_u$')
    plt.vlines(eps_d_avg, y_min, y_max, color='r', alpha=.1, label=r'$\bar{\epsilon}_d \, \mathrm{[Blandford]}$')
    plt.vlines(eps_d_avg_from_spec, y_min, y_max, color='c', alpha=.2, linestyle='--', label=r'$\bar{\epsilon}_d \, \mathrm{[from \, spec.]}$')

    plt.ylim(y_min, y_max)
    plt.xlim(min(epss), max(epss))
    plt.plot(epss, nuFnus, 'k')
    plt.legend(loc='best')
    plt.loglog()
    plt.xlabel(r'$\epsilon = E/m_e c^2$')
    plt.ylabel(r'$\nu F_\nu \, \mathrm{(arb. \, units.)}$')
    plt.tight_layout()
    if filename:
        plt.savefig(filename)
    else:
        plt.show()
    plt.close()
