import argparse
import numpy as np
import helpers
import physics

def get_default_parameters():
    # Default parameters
    theta_RMS = 4.44444e-4
    R_theta = theta_RMS/1e-6
    eps_d = 1.6353e-5
    return theta_RMS, R_theta, eps_d

def get_beta(theta_RMS):
    # Find normalization used
    A = physics.get_theta_RMS(1.)
    beta = np.sqrt(theta_RMS/A)
    return beta

def get_zeta(beta, theta_u, eps_d):
    # Compute zeta = n_gamma/n_p
    eps_u = 3.*theta_u
    A = (1./8.)*(47. - 49*eps_d/eps_u)
    M_sq = -A/2. + np.sqrt((A/2.)*(A/2.) + 3./4.)
    m_p = 1836.
    m_e = 1.
    zeta = (3./2.)*(m_p/m_e)*beta*beta/theta_u/M_sq
    return zeta

def get_args():
    # Get terminal input
    theta_RMS, R_theta, eps_d = get_default_parameters()
    parser = argparse.ArgumentParser(description='converts Kompaneets parameters to the corresponding RMS parameters. (the default parameters give beta_ud=0.1, theta_u=1e-6, zeta=1e6.)')
    parser.add_argument('-tr', '--theta-rms', type=float, default=theta_RMS, help='the RMS effective temperature [default=%4.4e' % (theta_RMS)+']')
    parser.add_argument('-R', '--R-theta', type=float, default=R_theta, help='the temperature ratio, R_theta = theta_RMS/theta_u [default=%4.4e' % (R_theta)+']')
    parser.add_argument('-ed', '--eps-d', type=float, default=eps_d, help='average downstream photon energy [default=%4.4e' % (eps_d)+']')
    parser.add_argument('-pd', '--print-default', action='store_true', help='instead, print default parameter values and exit')
    args = parser.parse_args()
    return args

def main():
    # Get the args
    args = get_args()

    # Print default values and exit?
    if args.print_default:
        print('default input parameters:')
        theta_RMS, R_theta, eps_d = get_default_parameters()
        helpers.print_parameter('theta_RMS', theta_RMS)
        helpers.print_parameter('R_theta', R_theta)
        helpers.print_parameter('eps_d', eps_d)
        raise SystemExit

    # Set the parameters from the args
    theta_RMS = args.theta_rms
    R_theta = args.R_theta
    eps_d = args.eps_d

    # Compute the corresponding RMS parameters
    beta = get_beta(theta_RMS)
    theta_u = theta_RMS/R_theta
    zeta = get_zeta(beta, theta_u, eps_d)

    # Print output
    print('rms parameters:')
    helpers.print_parameter('beta_ud', beta)
    helpers.print_parameter('theta_u', theta_u)
    helpers.print_parameter('zeta', zeta)

if __name__ == '__main__':
    main()
