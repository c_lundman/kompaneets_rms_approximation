import argparse
import numpy as np
import helpers
import physics
import plot

def get_default_parameters():
    # Default parameters
    beta = 0.1
    theta_u = 3e-6
    zeta = 1e5
    tau = 300.
    return beta, theta_u, zeta, tau

def get_args():
    # Get terminal input
    beta, theta_u, zeta, tau = get_default_parameters()
    parser = argparse.ArgumentParser(description='converts the RMS parameters to corresponding Kompaneets parameters, and plot the (approximate) spectrum')

    parser.add_argument('-b', '--beta', type=float, default=beta, help='the relative speed between the RMS up- and downstreams [default=%4.4e' % (beta)+']')
    parser.add_argument('-tu', '--theta-u', type=float, default=theta_u, help='the upstream temperature [default=%4.4e' % (theta_u)+']')
    parser.add_argument('-z', '--zeta', type=float, default=zeta, help='the photon-to-proton ratio [default=%4.4e' % (zeta)+']')
    parser.add_argument('-t', '--tau', type=float, default=tau, help='the optical depth [default=%4.4e' % (tau)+']')

    parser.add_argument('-bb', '--add-blackbodies', action='store_true', help='put blackbodies at the end of the computed spectrum, to make the figure look nicer:)')

    parser.add_argument('-pd', '--print-default', action='store_true', help='instead, print default parameter values and exit')
    parser.add_argument('-pk', '--print-kompaneets', action='store_true', help='instead, print the computed Kompaneets parameters and exit')
    parser.add_argument('-ps', '--print-spec', action='store_true', help='instead, print spectrum information and exit')

    parser.add_argument('-o', default=None, help='save plot to this file (instead of showing the plot)')
    args = parser.parse_args()
    return args

def main():
    # Get the args
    args = get_args()

    # Print default values and exit?
    if args.print_default:
        print('default input parameters:')
        beta, theta_u, zeta, tau = get_default_parameters()
        helpers.print_parameter('beta_default', beta)
        helpers.print_parameter('theta_u_default', theta_u)
        helpers.print_parameter('zeta_default', zeta)
        helpers.print_parameter('tau_default', tau)
        raise SystemExit

    # Set the parameters from the args
    beta = args.beta
    theta_u = args.theta_u
    zeta = args.zeta
    tau = args.tau

    # Find energy breaks
    eps_u_avg = physics.get_eps_u_avg_from_theta_u(theta_u)
    eps_d_min = physics.get_eps_d_min_from_theta_u(theta_u)
    eps_d_max = physics.get_eps_d_max_from_beta(beta)

    # Find Mach number and compute average energy gain
    M_sq = physics.get_M_sq(beta, theta_u, zeta)
    eps_d_avg = physics.get_eps_d_avg_over_eps_u_avg(M_sq)*eps_u_avg

    # Is there even a shock solution?
    if M_sq < 1:
        print("Error: Mach number < 1 (M^2 = %4.2f), no shock solution" % (M_sq))
        raise SystemExit

    # Print spectrum information?
    if args.print_spec:
        print('spectrum information:')
        helpers.print_parameter('eps_u_avg', eps_u_avg)
        helpers.print_parameter('eps_d_min', eps_d_min)
        helpers.print_parameter('eps_d_max', eps_d_max)
        helpers.print_parameter('eps_d_avg', eps_d_avg)
        helpers.print_parameter('eps_d_max/eps_d_min', eps_d_max/eps_d_min)
        helpers.print_parameter('theta_RMS', physics.get_theta_RMS(beta))

    # Compute power law index that gives the correct average energy gain
    alpha = physics.get_alpha(eps_d_min, eps_d_max, eps_d_avg/eps_d_min)

    # Get Kompaneets parameters
    y = physics.get_y(alpha)
    R_theta = physics.get_R_theta(beta, theta_u)
    tau_theta_RMS = tau*physics.get_theta_RMS(beta)

    # Print spectrum information and exit?
    if args.print_spec:
        helpers.print_parameter('alpha', alpha)
        helpers.print_parameter('nuFnu spec. index', alpha+4.)
        helpers.print_parameter('nuFnu(eps_d_max)/nuFnu(eps_d_min)', (eps_d_max/eps_d_min)**(alpha+4.))
        raise SystemExit

    # Print Kompaneets and exit?
    if args.print_kompaneets:
        print('output Kompaneets parameters:')
        helpers.print_parameter('tau*theta_RMS', tau_theta_RMS)
        helpers.print_parameter('R_theta', R_theta)
        helpers.print_parameter('y', y)
        raise SystemExit

    # Plot the estimated spectrum
    param_texts = plot.make_parameter_texts(beta, theta_u, zeta, tau, tau_theta_RMS, R_theta, y)
    epss, nuFnus, eps_d_avg_from_spec = plot.make_spectrum(eps_d_min, eps_d_max, alpha, add_blackbodies=args.add_blackbodies)
    plot.plot_spectrum(epss, nuFnus, eps_u_avg, eps_d_avg, eps_d_avg_from_spec, param_texts, filename=args.o)

if __name__ == '__main__':
    main()
