import numpy as np

def print_parameter(string, value, w=22):
    print('  '+string.ljust(w)+' = %8.4e' % (value))

def get_eps_avg_from_spec(epss, nuFnus):
    dlneps = epss[1]/epss[0]
    U = 0.
    N = 0.
    for i in xrange(len(epss)):
        eps = epss[i]
        dU = nuFnus[i]*dlneps
        dN = (nuFnus[i]/eps)*dlneps
        U += dU
        N += dN
    eps_avg = U/N
    return eps_avg

def blackbody(eps, theta, A=1.):
    x = eps/theta
    return A*(1./4.775)*x*x*x*x/(np.exp(eps/theta)-1.)

def nice_notation(x, ndp=2):
    s = '{x:0.{ndp:d}e}'.format(x=x, ndp=ndp)
    m, e = s.split('e')
    return r'{m:s}\times 10^{{{e:d}}}'.format(m=m, e=int(e))
